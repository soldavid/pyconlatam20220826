# [Python en la Nube: Automatizando la operación de AWS con Boto3](https://pylatam.org)

## PyCon Latam 2022 - 2022-08-26

[![PyConLatam 2022](images/pyconlatam-hero.png)](ttps://pylatam.org)

## David Sol

![David Sol](images/david-sol.jpg)

### Wizeline Site Reliability Engineer

### Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)

### Repositorio: <https://gitlab.com/soldavid/pyconlatam20220826>
